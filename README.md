# Tariff Analysis Cli Tool
`analyzer.py` is Python3 shell script prototype of a tariff analysis tool. It's just a proof of concept / mock up at this stage. It's limited to calculating *volum* usage charges only. The important concepts it is suppoed to demonstrate are:

  + Loading and parsing a meter data file.
  + Representing a hierarchical volume tariffs.
  + Creating hierarchical *binnings* of a usage data in the same format as tariff data structures.

# Synopsis
Script requires an meter data file with a year worth of data. Example:

> python3 analyzer.py -f ./data/sample-ami-meter-data-origin-20160401_20170401.csv -t alinta_fair_deal_43

# Installation
You need Python3 a some dependendencies installed. See requirements.txt. To install:

    pip install -r requirements.txt

## Installation with VirtualEnv
For development you'll want ot use virtualenv. To install with virtualenv just setup a virtualenv first (requires installing virtualenv):

    virtualenv -p `which python3` .cli-dev
    source  .cli-dev/bin/activate
    pip install -r requirements
    # ...
    deactivate

# Configuration
n/a.
