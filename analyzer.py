import argparse
import logging
import sys
from ami_data_parser import *
from hbin import *
from tariffs import Tariff, get_tariff_by_name
from tariffs.interval_usage_structures import *
from tariffs.interval_tariff_structures import *


logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


def main():
    parser = argparse.ArgumentParser(description='Simple analysis of volume usage under a given electricity volume tariff')
    parser.add_argument('--file', '-f', dest='filename', action='store', required=True,
        help='File containing a year worth of smart meter interval readings')
    parser.add_argument('--tariff', '-t', dest='tariff', action='store', required=True,
        help='Name of tariff. Example \'alinta_fair_deal_43\'')
    args = parser.parse_args()

    interval_data = load_interval_data(args.filename)
    tariff = load_tariff(args.tariff)
    print('Parsed interval data (%d) and tariff' % (len(interval_data)))
    print_usage(interval_data)
    print_cost(interval_data, tariff)


def print_usage(interval_data):
    print('#### Usage Breakdown')
    q_usage_bin = HBin.build(standard_usage_structure)
    m_usage_bin = HBin.build(flat_monthly_structure)
    q_usage_bin = q_usage_bin.binList(interval_data.flat)
    m_usage_bin = m_usage_bin.binList(interval_data.flat)
    print(q_usage_bin.to_string())
    print(m_usage_bin.to_string())


def print_cost(interval_data, tariff):
    ''' Print costs ... '''
    print('###### Costs Under Tariff (%s) ############' % (tariff['name']))
    (outright_costs, temporal_costs) = tariff.apply(interval_data.flat).flat()
    print('### Out Right Costs ######\n' + str(outright_costs))
    print('### Total Yearly Costs ($/Y) ######\n' + str(temporal_costs.sum()))
    print('### Total Yearly Cost by Tariff Volume Charge Breakdown ######')
    print(tariff['volume_charge'][0]['charge'].binList(temporal_costs.values).to_string())


def load_interval_data(filename):
    interval_data = None
    try:
        interval_data = read_origin_interval_data_file(filename)
        interval_data = align_year(interval_data)  # Start at January 1st.
        interval_data = interval_data.iloc[:, 1:].values  # Turn into ndarray
    except Exception as e:
        print('Could not load %s. [%s]' % (filename, e))
        sys.exit()
    print('Loaded interval data %s. %s' % (filename, len(interval_data)))
    return interval_data


def load_tariff(tariff_name):
    tariff = None
    try:
        tariff = get_tariff_by_name(tariff_name)
    except Exception as e:
        print('Could not load tariff "%s" [%s]' % (tariff_name, e))
        sys.exit()
    print('Loaded tariff')
    return tariff


main()
